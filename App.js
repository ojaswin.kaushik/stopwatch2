import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View ,FlatList} from "react-native";
import { Double } from "./components/test.js";
import Timer from "./components/timer.js";
import Start from "./components/start.js";
import Pause from "./components/pause.js";
import Lap from "./components/laps.js";
import Restart from "./components/restart.js";
import { useState, useEffect } from "react";

export default function App() {
  const [hour, setHour] = useState("0");
  const [min, setMin] = useState("0");
  const [sec, setSec] = useState("0");
  const [mili, setMili] = useState("0");
  const [flag, setFlag] = useState(0);
  const [laps,setLaps]  = useState([])

  function Update_Timer(x, y, z, w, mod) {
    // 1 sec  = 1000
    // 1 hour = 60  min
    //console.log(sec)
    if ((Number(x) + 1) / y == 1) {
      z((+w + 1) % mod);
    }
  }
  useEffect(() => {
    setTimeout(() => {
      if (flag === 2) {
        let new_sec = (Number(mili) + 1) / 100;
        if (new_sec === 1) {
          // Update minute
          let new_minute = (Number(sec) + new_sec) / 60;
          if (new_minute === 1) {
            // Updating Hour
            let new_hour = (Number(min) + new_minute) / 60;

            if (new_hour === 1) setHour((Number(hour) + new_hour) % 60);

            setMin((Number(min) + new_minute) % 60);
          }
          setSec((Number(sec) + new_sec) % 60);
        }

        let new_mili = (Number(mili) + 1) % 100;

        setMili(new_mili);
      }
    }, 10);
  }, [flag, mili]);

  return (
    <View style={styles.container}>
      <View>
        <Timer hour={hour} min={min} sec={sec} mili={mili}></Timer>
      </View>
      <View style={styles.button}>

        <Start  flag={flag} setFlag={setFlag}></Start>

        <Pause flag={flag} setFlag={setFlag}></Pause>

        <Restart flag={flag} setHour={setHour} setMili={setMili} setMin={setMin} 
        setSec={setSec} setLaps={setLaps} setFlag={setFlag}></Restart>

        <Lap hour={hour} min={min} mili ={mili} sec={sec} setLaps={setLaps} laps={laps}></Lap>

      </View>
      <View>
      <FlatList data={laps} key={laps.length}
       renderItem={
        ({item})=>(
          <Text style={styles.text}>{Double(item[1])} : {Double(item[2])} : {Double(item[3])} : {Double(item[4])}</Text>
        )
      }></FlatList>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#6E501E",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "gold",
  },
  button: {
    flexDirection: "row",
  },
});
