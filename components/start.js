import { StyleSheet, Text, View ,Button} from 'react-native';

import { useState } from 'react';

const Start = (props) => {
    function f()
    {
        props.setFlag(2)
    }
    return ( <View style={styles.btn}>
        <Button disabled={props.flag===2} onPress={f} title='Start' color="coral"></Button>
    </View> );
}

const styles = StyleSheet.create({
    btn:
    {
        margin:10,
        width:100,
        
    }
})
 
export default Start;