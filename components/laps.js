import { StyleSheet, Text, View ,Button} from 'react-native';

import { useState } from 'react';

const Lap = (props) => {
    function AddLaps()
    {
        
        props.setLaps([...props.laps,[props.laps.length+1,props.hour,props.min,props.sec,props.mili]])

        
    }
    return ( <View style={styles.btn}>
        <Button onPress ={AddLaps} title='Laps' color='coral'>
            </Button> 
    </View>);
}
 
const styles = StyleSheet.create({
    btn:
    {
        margin:10,
        width:100,
        
    }
})
export default Lap;