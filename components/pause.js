import { StyleSheet, Text, View,Button } from 'react-native';

import { useState } from 'react';

const Pause = (props) => {
    return (  <View style={styles.btn}>
        <Button disabled={(props.flag===1 || props.flag===3)} onPress ={()=>
        {
            props.setFlag(3)
        }}title='Pause' color='coral'></Button>
        </View>
        );
}
const styles = StyleSheet.create({
    btn:
    {
        margin:10,
        width:100,
        
    }
})
 
export default Pause;