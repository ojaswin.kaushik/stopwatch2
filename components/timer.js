import { StyleSheet, Text, View } from 'react-native';

import { Double } from "./test.js";
import { useState } from 'react';


const Timer = (props) => {
    return (  
        <View style={styles.timer}>
            <Text  style={styles.text}>{Double(props.hour)} :</Text>
            <Text style={styles.text}>{Double(props.min)} :</Text>
            <Text style={styles.text}>{Double(props.sec)} :</Text>
            <Text style={styles.text}>{Double(props.mili)} </Text>

        </View>);
}

const styles = StyleSheet.create({
    text:
    {
      color:"white",
      letterSpacing:2,
      margin:5,
      padding:11,
      fontSize:18,
    },
    timer:
    {
        flex:1,
        flexDirection:'row',
        padding:5
    }
  });
  
 
export default Timer;