import { StyleSheet, Text, View ,Button} from 'react-native';

import { useState } from 'react';

const Restart = (props) => {
    function Reset()
    {
        props.setHour(0)
        props.setMin(0)
        props.setSec(0)
        props.setMili(0)
        props.setFlag(0)
        props.setLaps([])
        
    }
    return ( <View style={styles.btn}><Button disabled={(props.flag===2 || props.flag===1)} onPress={Reset}title="Restart" color='coral'></Button></View> );
}
 
const styles = StyleSheet.create({
    btn:
    {
        margin:10,
        width:100,
        
    }
})
export default Restart;